import os
import time
import threading

class TimedOutExc(Exception):
    pass

class ActionExc(Exception):
    pass

class ReturnThread(threading.Thread):

    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs={}, Verbose=None):
        threading.Thread.__init__(self, group, target, name, args, kwargs)
        self._return = None

    def run(self):
        try:
            if self._target is not None:
                self._return = self._target(
                    *self._args,
                    **self._kwargs
                )
        except:
            # NOTE: this means there was an error in the agent
            self._return = None

    def join(self, *args, **kwargs):
        threading.Thread.join(self, *args, **kwargs)
        return self._return

def timeout(time_limit):
    def timeout_inner(func):
        def inner_func(*args, **kwargs):
            t = ReturnThread(target=func, args=args, kwargs=kwargs)
            t.start()
            return_vals = t.join(timeout=time_limit)
            if t.isAlive():
                # NOTE: then it timed out
                raise TimedOutExc
            elif return_vals is None:
                raise ActionExc
            else:
                return return_vals
        return inner_func
    return timeout_inner

def prettify_action(action_type, string_):
    if action_type == "add_to_start":
        return "add the character '" + string_.upper() + "' to the start of the string"
    elif action_type == "add_to_end":
        return "add the character '" + string_.upper() + "' to the end of the string"
    elif action_type == "challenge_no_word":
        return "challenge that no word could be formed"
    elif action_type == "challenge_is_word":
        return "challenge that the string is already a word"
    elif action_type == "claim_word":
        return "claim that '" + string_.upper() + "' is a valid word"

def prettify_loss_condition(loss_condition):
    if loss_condition == "formed_word":
        return "forming a valid word"
    elif loss_condition == "challenge_no_word_failed":
        return "challenging a valid substring"
    elif loss_condition == "claim_word_failed":
        return "not being able to provide a valid word using the string"
    elif loss_condition == "invalid_action":
        return "trying an invalid action"

def pretty_print_state(state_, players):
    print("-------------------")
    if state_["last_turn"] is None:
        print("Game Started!")
        return

    print(
        "Player {} decided to {}.".format(
            players[state_["last_turn"]].name, 
            prettify_action(*state_["last_action"])
        )
    )
    
    if state_["loser"] is not None:
        print("Player {} lost the game due to {}.".format(players[state_["loser"]].name, prettify_loss_condition(state_["loss_condition"])))
    else:
        print("The resulting string is '{}'.".format(state_["current_string"]))

if __name__ == "__main__":

    @timeout(3)
    def add(a_, b_):
        time.sleep(10)
        return a_ + b_

    try:
        print(add(3,4))
    except TimedOutExc:
        print("timed out!")
    print("next thing!")