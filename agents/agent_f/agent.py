'''
Implement your team agent here
To test your agent, you can use a script like run_game.py
    to let it play against itself or other agents you might have.
    You'll have to modify that script abit.
For reference, there is a sample agent in agent.py.
If you have other files you need (like maybe neural net weights?), you can create an additional folder and dump those there.
You can add any other functions and code here.
We will just always call PlayerAgent.__init__ and PlayerAgent.get_action
Make sure your code will work in the environment specified in environment.yml
'''

import os
import random
import numpy as np
try:
    from keyword import KeywordProcessor
except:
    from agents.agent_f.keyword import KeywordProcessor

from agent import WordntAgent

class Agent(WordntAgent):
    '''
    Keep this class named 'Agent' please. I will import it to the game.
    This is just a sample agent to show how it should respond.
    This probably won't perform very well.
    '''

    _LETTERS = 'abcdefghijklmnopqrstuvwxyz'

    def __init__(self, **kwargs):
        '''
        You can also add code here.
        '''

        super().__init__(**kwargs)
        self.n_to_check = kwargs.get("n_to_check", 10000)
        random.shuffle(self._words)
        
        self._LETTERS = 'abcdefghijklmnopqrstuvwxyz'.upper()
        self.letters = list(self._LETTERS)
        self.search = KeywordProcessor()
        self.search.add_keyword_from_file('data/wordnt_words.txt')
        
    def generateAndSearch(self, string):
        string = string.upper()
        finalString = ''
        generated = []
        valid = []
        invalid_1c = []
        valid_2c = []

        # Generate all new strings that can be formed
        # by adding 1 character at the start or end
        for j in self.letters:
            generated.append(string + j)
            generated.append(j + string)
        joined = ' '.join(generated)

        # Extract generated strings that are invalid
        # (and can be played safely)
        valid = self.search.extract_keywords(joined)
        invalid_1c = np.setdiff1d(generated, valid)

        # Determine generated invalid strings that
        # can still be extended to form valid strings
        for j, inv in enumerate(invalid_1c):
            for i, word in enumerate(self._words):
                if inv in word:
                    valid_2c.append(inv)
        valid_2c = list(dict.fromkeys(valid_2c))

        # Get the first extendable invalid string
        # and return the action to create it
        if len(valid_2c) > 0:
            finalString = valid_2c[0][:]
            if finalString[1:]==string:
                return 'add_to_start', finalString[0].lower()
            else: 
                return 'add_to_end', finalString[-1].lower()
        else:
            return 'challenge_no_word', None
        return generated, valid, invalid_1c, valid_2c, finalString

    def get_action(self, game_state):
        '''
        This is where you implement your AI.
        It should output actions based on the game state.
        Game state is a dictionary as follows:
            {
                "current_turn": int. the index of the player currently doing an action,
                "last_turn": int or None. the index of the player who did the previous action,
                "current_string": str. the current string,
                "order_added": list of int. indicates the order in which characters were added to the current string,
                "last_action": tuple or None. the action of the previous player,
                "done": bool. indicates if the game is finished,
                "loser": int or None. the index of the player who lost the game,
                "loss_condition": str or None. describes how the loser lost the game,
            }
        Refer to the SampleAgent in agent.py for the syntax of output actions.
        Output action is a tuple of two items:
            action_type: str. any of:
                "add_to_start": add string_ to the start of the current string
                "add_to_end": add _string to the end of the current string
                "challenge_no_word": challenge the previous player to prove that a valid word can be produced from the current string
                "challenge_is_word": challenge that the current string is itself a valid word
                "claim_word": respond to 'challenge_no_word' by presenting a supposedly valid word from the current string
            string_: str or None.
                if action_type is "add_to_start" or "add_to_end", the character to add
                if action_type is "challenge_no_word" or "challenge_is_word", None
                if action_type is "claim_word", the supposedly valid word
        Remember that your agent will lose the game automatically if
            this method takes too long or has any error.
        '''

        # Add random letter to start the game
        if game_state["last_action"] is None:
            return "add_to_start", random.choice(self._LETTERS)

        # If challenged that there are no more words,
        # Look for the first instance of a substring
        elif game_state["last_action"][0] == "challenge_no_word":
            for i, word in enumerate(self._words):
                if game_state["current_string"] in word:
                    return "claim_word", word
            return "claim_word", game_state["current_string"] + "S"

        # If the current string is a valid word,
        # Challenge the current string
        elif game_state["current_string"] in self._words and len(game_state["current_string"]) >= 3:
            return "challenge_is_word", None

        # Otherwise, add a letter to the current string
        else:
            return self.generateAndSearch(game_state["current_string"])
        return "challenge_no_word", None