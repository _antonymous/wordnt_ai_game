'''
Implement your team agent here
To test your agent, you can use a script like run_game.py
    to let it play against itself or other agents you might have.
    You'll have to modify that script abit.
For reference, there is a sample agent in agent.py.
If you have other files you need (like maybe neural net weights?), you can create an additional folder and dump those there.
You can add any other functions and code here.
We will just always call PlayerAgent.__init__ and PlayerAgent.get_action
Make sure your code will work in the environment specified in environment.yml
'''

import os
import random

from agent import WordntAgent
from .best_proposal_finder import find_best_proposal

class Agent(WordntAgent):
    '''
    Keep this class named 'Agent' please. I will import it to the game.
    This is just a sample agent to show how it should respond.
    This probably won't perform very well.
    '''

    def __init__(self, **kwargs):
        '''
        You can also add code here.
        '''

        # self._words is initialized here
        # self.n_players is also initialized here
        super().__init__(**kwargs)

        self._max_word_len = max([len(word) for word in self._words])
        self._words_set = set(self._words)
        self._letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        self._latest_basis_words = set()

    def get_action(self, game_state):
        '''
        This is where you implement your AI.
        It should output actions based on the game state.
        Game state is a dictionary as follows:
            {
                "current_turn": int. the index of the player currently doing an action,
                "last_turn": int or None. the index of the player who did the previous action,
                "current_string": str. the current string,
                "order_added": list of int. indicates the order in which characters were added to the current string,
                "last_action": tuple or None. the action of the previous player,
                "done": bool. indicates if the game is finished,
                "loser": int or None. the index of the player who lost the game,
                "loss_condition": str or None. describes how the loser lost the game,
            }
        Refer to the SampleAgent in agent.py for the syntax of output actions.
        Output action is a tuple of two items:
            action_type: str. any of:
                "add_to_start": add string_ to the start of the current string
                "add_to_end": add _string to the end of the current string
                "challenge_no_word": challenge the previous player to prove that a valid word can be produced from the current string
                "challenge_is_word": challenge that the current string is itself a valid word
                "claim_word": respond to 'challenge_no_word' by presenting a supposedly valid word from the current string
            string_: str or None.
                if action_type is "add_to_start" or "add_to_end", the character to add
                if action_type is "challenge_no_word" or "challenge_is_word", None
                if action_type is "claim_word", the supposedly valid word
        Remember that your agent will lose the game automatically if
            this method takes too long or has any error.
        '''

        # responding to a challenge
        if game_state["last_action"] is not None:
            if game_state["last_action"][0] == "challenge_no_word":
                if self._latest_basis_words:
                    action_string = random.choice(list(self._latest_basis_words))
                else:
                    action_string = random.choice(self._words)
                action_type = "claim_word"

                return action_type, action_string
    
        # where all the magic happens
        output_summary = find_best_proposal(game_state["current_string"], self.n_players, self._max_word_len, self._words_set, self._letters,
                                            use_metagame_strat = True)
                                            
        action_type, action_string = output_summary["action_type"], output_summary["action_string"]

        # update basis words
        if output_summary["best_proposal_basis_words"] is not None:
            self._latest_basis_words = output_summary["best_proposal_basis_words"]
        
        return action_type, action_string