'''
Implement your team agent here
To test your agent, you can use a script like run_game.py
    to let it play against itself or other agents you might have.
    You'll have to modify that script abit.
For reference, there is a sample agent in agent.py.
If you have other files you need (like maybe neural net weights?), you can create an additional folder and dump those there.
You can add any other functions and code here.
We will just always call PlayerAgent.__init__ and PlayerAgent.get_action
Make sure your code will work in the environment specified in environment.yml
'''

import os
import random
from functools import reduce

from agent import WordntAgent

class Agent(WordntAgent):
    '''
    YAM NOTES:
    Keep this class named 'Agent' please. I will import it to the game.
    This is just a sample agent to show how it should respond.
    This probably won't perform very well.
    '''

    _LETTERS = ('abcdefghijklmnopqrstuvwxyz').upper()

    def __init__(self, **kwargs):

        super().__init__(**kwargs)

        #self.n_to_check = kwargs.get("n_to_check", 10000)

        # NOTE: random.shuffle only workds inplace
        #random.shuffle(self._words)

        # self._validWords = [word for word in self._words]

        self._longestWordLength = len(max(self._words, key=len))

    def get_action(self, game_state):
        '''
        This is where you implement your AI.
        It should output actions based on the game state.
        Game state is a dictionary as follows:
            {
                "current_turn": int. the index of the player currently doing an action,
                "last_turn": int or None. the index of the player who did the previous action,
                "current_string": str. the current string,
                "order_added": list of int. indicates the order in which characters were added to the current string,
                "last_action": tuple or None. the action of the previous player,
                "done": bool. indicates if the game is finished,
                "loser": int or None. the index of the player who lost the game,
                "loss_condition": str or None. describes how the loser lost the game,
            }
        Refer to the SampleAgent in agent.py for the syntax of output actions.
        Output action is a tuple of two items:
            action_type: str. any of:
                "add_to_start": add string_ to the start of the current string
                "add_to_end": add _string to the end of the current string
                "challenge_no_word": challenge the previous player to prove that a valid word can be produced from the current string
                "challenge_is_word": challenge that the current string is itself a valid word
                "claim_word": respond to 'challenge_no_word' by presenting a supposedly valid word from the current string
            string_: str or None.
                if action_type is "add_to_start" or "add_to_end", the character to add
                if action_type is "challenge_no_word" or "challenge_is_word", None
                if action_type is "claim_word", the supposedly valid word
        Remember that your agent will lose the game automatically if
            this method takes too long or has any error.
        '''

        # print("YAAAAAY")

        if game_state["last_action"] is None:
            # NOTE: game just started
            dangerWordLens = []
            currentDangerWordLength = 0
            counter = 1
            while currentDangerWordLength < self._longestWordLength:
                currentDangerWordLength = len(game_state["current_string"]) + (self.n_players * counter) + 1
                dangerWordLens.append(currentDangerWordLength)
                counter += 1
            dangerWordLens = list(set(dangerWordLens))
            # print("Danger Word Lengths:")
            # print(dangerWordLens)

            minDangerRatio = 1.00
            actualLetter = '0' #lmao by default
            for c in self._LETTERS:
                #should give me number of words that letter appears in
                letterPresence = reduce(lambda count, word: count + ( c in (word).upper() ), self._words, 0) 
                dangerousLetterPresence = reduce(lambda count, word: count + ( c in (word).upper() and len(word) in dangerWordLens ), self._words, 0) 
                if letterPresence > 0:
                    riskFactor = dangerousLetterPresence/letterPresence
                    if riskFactor < minDangerRatio:
                        minDangerRatio = riskFactor
                        actualLetter = c          

            if actualLetter == '0':
                print("this should NEVER fire")
                return "challenge_no_word", None
            else:
                return "add_to_start", str(actualLetter)
        elif game_state["last_action"][0] == "challenge_no_word":
            # NOTE: need to respond to challenge
            # NOTE: just find the first word that contains the string
            for i, word in enumerate(self._words):
                if game_state["current_string"] in word:
                    return "claim_word", word
            return "claim_word", game_state["current_string"] + 'S'
        elif game_state["current_string"] in self._words and len(game_state["current_string"]) >= 3:
            # NOTE: if string is a word, challenge it
            return "challenge_is_word", None
        else: #means game has started, and not challenge-able, so gotta add a letter
            #find all valid words given the current string
            self._validWords = [word for word in self._words if game_state["current_string"] in word]

            sampleEntries = {}
            lengthsOfValidWords = {}

            if len(self._validWords) == 0: #cant find any valid words for the string lmao
                return "challenge_no_word", None
            
            #check prepending

            #get list of valid substrings that are NOT whole words
            for c in self._LETTERS:
                dummyWord = c + game_state["current_string"]
                if dummyWord in self._validWords:
                    #its a whole word and u should be scared
                    continue
                else:
                    dummyCount = reduce(lambda count, word: count + ( dummyWord in (word).upper() ), self._validWords, 0)
                    sampleEntries[dummyWord] = dummyCount
            #check appending
            for c in self._LETTERS:
                dummyWord = game_state["current_string"] + c
                if dummyWord in self._validWords:
                    #its a whole word and u should be scared
                    continue
                else:
                    dummyCount = reduce(lambda count, word: count + ( dummyWord in (word).upper() ), self._validWords, 0)
                    sampleEntries[dummyWord] = dummyCount

            #remove Entries with values 0, means no valid words with those substrings              
            sampleEntries = {k:v for k,v in sampleEntries.items() if v != 0}

            if len(sampleEntries) == 0:
                print("everything can be plural if you try hard enough")
                return "add_to_end", 'S'

            for key, value in sampleEntries.items():
                for word in self._validWords:
                    if key not in lengthsOfValidWords:
                        lengthsOfValidWords[key] = []
                    if key in word:
                        lengthsOfValidWords[key].append(len(word))
                lengthsOfValidWords[key] = list(set(lengthsOfValidWords[key]))
            # print("Lengths of Valid Words based on substring options")
            # print(lengthsOfValidWords)

            #figure out which substrings are unique to the shortest words, making sure you dont kill yourelf in the process haha
            dangerWordLens = []
            currentDangerWordLength = 0
            counter = 1
            while currentDangerWordLength < self._longestWordLength:
                currentDangerWordLength = len(game_state["current_string"]) + (self.n_players * counter) + 1
                dangerWordLens.append(currentDangerWordLength)
                counter += 1
            dangerWordLens = list(set(dangerWordLens))
            # print("Danger Word Lengths:")
            # print(dangerWordLens)

            safeOptions = {}
            safeOptions = {k:v for k,v in lengthsOfValidWords.items() if len(set(v).intersection(set(dangerWordLens))) == 0}
            # print("safe options:")
            # print(safeOptions)

            finalSubstring = ""
            if len(safeOptions) > 0:
                #AWW YISS
                print("AWW YISS")
                for key, value in safeOptions.items():
                    finalSubstring = key
                    break
            else:
                #awtsu :(
                print("awtsu :(")
                currentMin = 1.00
                for k,v in sampleEntries.items():
                    wordsYouCanMake = [word for word in self._validWords if k in word]
                    backfireRatio = len([badWord for badWord in wordsYouCanMake if len(badWord) in dangerWordLens]) / len(wordsYouCanMake)
                    # print("backfire ratio")
                    # print(k + ": " + str(backfireRatio))
                    safeWordRatio = 1 - backfireRatio
                    if safeWordRatio < currentMin:
                        # print("safe word lol")
                        # print(safeWordRatio)
                        currentMin = safeWordRatio
                        finalSubstring = k
            # print("final substring")
            # print(finalSubstring)
            
            if game_state["current_string"] in finalSubstring:
                substring_start = finalSubstring.find(game_state["current_string"])

                if substring_start == 0:
                    # NOTE: base string is at the start of the word, add to end of the word
                    return "add_to_end", finalSubstring[-1]

                else:
                    # NOTE: base string is at the end of the word, add to beginning of the word
                    return "add_to_start", finalSubstring[0]