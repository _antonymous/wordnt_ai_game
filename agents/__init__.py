from .sample_agent import Agent as SampleAgent
# TODO: fill this up when teams submit their agents

from .agent_a import Agent as AgentA
from .agent_b import Agent as AgentB
from .agent_c import Agent as AgentC
from .agent_d import Agent as AgentD
from .agent_e import Agent as AgentE
from .agent_f import Agent as AgentF